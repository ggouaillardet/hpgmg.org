Title: Updated HPGMG-FV ranking
Category:
Tags: results, versatility
Date: 2015-02-06
Summary: Updated HPGMG-FV scaling results and rankings.

We recently received our first external HPGMG-FV submission, from BiFrost at NSC.
See the [HPGMG-FV Submission Guidelines](http://crd.lbl.gov/departments/computer-science/performance-and-algorithms-research/research/hpgmg/) if you would like to add your machine to the rankings.
Here are the current rankings:

![HPGMG-FV rankings, 2015 January](/images/hpgmg-fv-500-201501.png)

Note that some machines may be significantly under-represented due to having run on a small fraction of the machine.

## Performance Versatility
Scalability depends crucially on problem size, thus time to solution.
Applications such as climate, subsurface flow, statistically-converged large eddy simulation, and molecular dynamics require many time steps and, due to external requirements on time to solution, are typically run near their strong scaling limit.
Such applications run best on machines with good performance at fast time to solution.
Other applications, such as certain types of data analysis, are constrained by total available memory.

### HPGMG-FV
This scatter plot shows performance versus solve time for the HPGMG-FV configurations run, with the area of each point indicating the problem size per thread.
Hover over points for details about the machine and configuration.

<div id="fv-chart">
  <svg></svg>
</div>

This plot has only one data point per machine, typically not at the strong scaling limit or memory-capacity limit, thus provides information only about that single configuration and not the performance spectrum of each machine.

### HPGMG-FE
We have broader performance spectrum data available for HPGMG-FE, though on fewer machines; here we show for Edison, SuperMUC, and Titan (CPU-only).
The cluster of samples for each problem size represents performance variability.
Hover over points for more details about the sample.

<div id="fe-chart">
  <svg></svg>
</div>

Note that GDOF/s is not directly comparable between the FV and FE implementations since the latter is higher order and uses general mapped grids leading to increased data requirements for coordinates and much higher arithmetic intensity.
The configurations run on Edison and SuperMUC used a power of 2 cores per node, but HPGMG-FE does not require this and has sufficiently high arithmetic intensity to benefit from using all cores.

<script>
// create the chart
var fvchart,fechart; // Global variables to enable debugging in console
function chart(chart, path, id, threshold, custom) {
d3.csv(path)
  .row(function(d) {
    dofperthread = +d["DOF/Process"]/+d["OMP Threads/Rank"];
    return {label: d["System"] + ' (' + d["Fraction of System"] + ')',
      time: (+d["DOF/Process"])*(+d["MPI Ranks"])/(+d["DOF/s"]), dofps: +d["DOF/s"]*1e-9,
      mpi: +d["MPI Ranks"], openmp: +d["OMP Threads/Rank"],
      gdof: +d["DOF/Process"]*(+d["MPI Ranks"])*1e-9,
      size: dofperthread*1e-5};
  })
  .get(function(error, rows) {
    data = {};
    for (i in rows) {
      row = rows[i];
      if (data[row.label] == undefined) {data[row.label] = [];}
      if (row.time > threshold) continue; // Filter outliers and large runs
      data[row.label].push({x: row.time, y: row.dofps, mpi:row.mpi, openmp: row.openmp, gdof: row.gdof, size:row.size, shape: 'circle'});
    }
    datalist = []
    for (k in data) {
      datalist.push({key: k, values: data[k]})
    }
    ready(chart, datalist, id, custom);
  });
}
chart(fvchart, "/static/hpgmg-fv-500-201501.csv", '#fv-chart', 1000, function(c) {c.showLegend(false);})
chart(fechart, "/static/hpgmg-fe-201411.csv", '#fe-chart', 20)
function ready(chart, data, id, custom) {
  nv.addGraph(function() {
    chart = nv.models.scatterChart()
      .showDistX(true)
      .showDistY(true)
      .useVoronoi(true)
      .color(d3.scale.category10().range())
      .duration(300)
    ;
    chart.dispatch.on('renderEnd', function() {
      console.log('render complete: ' + id);
    });

    chart.xAxis.tickFormat(d3.format('.02f'));
    chart.yAxis.tickFormat(d3.format('.02f'));
    chart.xAxis.axisLabel("Solve Time (s)");
    chart.yAxis.axisLabel("Performance (GDOF/s)");
    chart.tooltipContent(function(key, x, y, tip, data) {
      return '<h3>' + key + '</h3> <p>' + d3.format('.02f')(data.point.gdof) + ' GDOF, ' + data.point.mpi + '*' + data.point.openmp + ' (MPI*OMP)</p>';
    });
    if (custom) {custom(chart);}
    d3.select(id + ' svg')
      .attr("height",400).attr("width",600)
      .datum(data)
      .call(chart);

    nv.utils.windowResize(chart.update);

    chart.dispatch.on('stateChange', function(e) { ('New State:', JSON.stringify(e)); });
    return chart;
  });
}
</script>

Please join the discussion on the [hpgmg-forum mailing list](/lists/listinfo/hpgmg-forum).
