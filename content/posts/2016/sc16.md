Title: SC16 BoF
Category: Results
Date: 2016-11-30
Summary: Slides from the HPGMG Birds of a Feather session at SC16

The HPGMG team held a Birds of a Feather session at SC16.  Slides are available:

* [Introduction and Overview](/static/SC16-HPGMG-BoF-Intro.pdf), HPGMG Team
* [Optimizations for Knight's Landing](/static/SC16-HPGMG-BoF-KNL.pdf), Sam Williams
* [NVIDIA Pascal GPU version](/static/SC16-HPGMG-BoF-NVIDIA.pdf), Peng Wang and Nikolay Sakharnykh
* [Communication and Vectorization analysis](/static/SC16-HPGMG-BoF-HLRS.pdf), Vladimir Marjanovic

These presentations were followed by a wide-ranging discussion.

For current rankings, see the [November 2016 Ranking List](https://crd.lbl.gov/departments/computer-science/PAR/research/hpgmg/results/results-201611/).
See [detailed submission guidelines](/submission) if you would like to contribute to this list.

