Title: SC14 BoF and Survey
Category:
Tags: community
Date: 2014-10-13
Summary: Please take a very short survey to help prepare for the SC14 BoF.

We are pleased to announce that the HPGMG team will be hosting a [birds of a feather (BoF) session](http://sc14.supercomputing.org/schedule/event_detail?evid=bof146) at Supercomputing on Wed. November 19, 12:15 - 1:15.

We will present a short overview of the project and design, followed by presentations from the team on the design of, and experience with, HPGMG and presentations from external users.  We will conclude with a discussion period to address question/comment from the community.

Anyone interested in presenting experience with HPGMG during this BoF is invited to contact us.  We have a 1 hour session and anticipate 2-5 short presentations from users.

# Short Survey

Please help us prioritize by completing a [one-page survey](https://www.surveymonkey.com/s/5LZZHQP).
